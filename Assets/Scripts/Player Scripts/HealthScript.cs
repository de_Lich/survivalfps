using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class HealthScript : MonoBehaviour
{
    private EnemyAnimator enemy_anim;
    private NavMeshAgent navAgent;
    private EnemyController enemy_Controller;

    public float health = 100f;
    public bool is_Player, is_Enemy;
    public bool is_Cannibal, is_Boar;
    private bool is_Dead;
    private EnemyAudio enemy_Audio;

    private PlayerStats player_Stats;

    private void Awake()
    {
        if (is_Enemy)
        {
            enemy_anim = GetComponent<EnemyAnimator>();
            enemy_Controller = GetComponent<EnemyController>();
            navAgent = GetComponent<NavMeshAgent>();
            enemy_Audio = GetComponentInChildren<EnemyAudio>();
        }
        if (is_Player)
        {
            player_Stats = GetComponent<PlayerStats>();
        }
    }
    void Start()
    {

    }
    public void ApplyDamage(float damage)
    {
        if (is_Dead) return;

        health -= damage;

        if (is_Player)
        {
            player_Stats.Display_HealthStats(health);
        }
        if (is_Enemy)
        {
            if(enemy_Controller.Enemy_State == EnemyState.PATROL)
            {
                enemy_Controller.chase_Distance = 50f;
            }
        }
        if(health <= 0f)
        {
            PlayerDead();
            is_Dead = true;
        }
    }
    private void PlayerDead()
    {
        if (is_Enemy)
        {
            navAgent.velocity = Vector3.zero;
            navAgent.isStopped = true;
            enemy_Controller.enabled = false;

            enemy_anim.Dead();
            StartCoroutine(DeadSound());
            if (is_Cannibal) 
            {
                EnemyManager.instance.EnemyDied(true);
            }
            if (is_Boar)
            {
                EnemyManager.instance.EnemyDied(false);
            }
        }
        if (is_Player)
        {
            GameObject[] enemies = GameObject.FindGameObjectsWithTag(Tags.ENEMY_TAG);

            for (int i = 0; i < enemies.Length; i++)
            {
                enemies[i].GetComponent<EnemyController>().enabled = false;
            }
            EnemyManager.instance.StopSpawning();

            GetComponent<PlayerMovement>().enabled = false;
            GetComponent<PlayerAttack>().enabled = false;
            GetComponent<WeaponManager>().GetCurrentSelectedWeapon().gameObject.SetActive(false);
        }
        if(tag == Tags.PLAYER_TAG)
        {
            Invoke("RestartGame", 3f);
        }
        else
        {
            Invoke("TurnOffGameObject", 3f);
        }
    }
    private void RestartGame()
    {
        SceneManager.LoadScene("SampleScene");
    }
    private void TurnOffGameObject()
    {
        gameObject.SetActive(false);
    }
    private IEnumerator DeadSound()
    {
        yield return  new WaitForSeconds(0.3f);
        enemy_Audio.Play_DeadSound();
    }
}
